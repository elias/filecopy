#include <stdio.h>

int main(int argc, char *argv[]){

        FILE *inF;
        inF = fopen(argv[1], "r");
            
        FILE *outF;
        outF = fopen(argv[2], "w");
        
        char buffer[4096];

        // Write Input in buffer
        int j;
        for(j = 0; buffer[j] != EOF; j++){
            int i;
            for(i = 0; buffer[i] < 4092 && buffer[i] != EOF; i++){
                buffer[i] = fgetc(inF);
            }
            
            fprintf(outF, buffer);
            printf("%s\n", buffer);
            if(buffer[j] == EOF){
                break;
            }
        }
    
    return 0;
}
